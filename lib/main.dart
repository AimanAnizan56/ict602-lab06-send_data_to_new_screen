import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import './second_screen.dart';

class Todo {
  final String title;
  final String description;

  Todo(this.title, this.description);
}

final todos = List <Todo>.generate(
    20,
    (index) => Todo(
      'Todo $index',
      'A description of what needs to be done for Todo $index')
);

void main() {
  runApp(MaterialApp(
    title: 'Navigation Basics',
    home: TodoScreen(),
  ));
}

class TodoScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('todos: Aiman bin Anizan'),
      ),
      body: Center(
        child: ListView.builder(
          itemCount: todos.length,
          itemBuilder: (context, index) {
            return ListTile(
              title: Text(todos[index].title),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => DetailScreen(todo: todos[index]))
                );
              },
            );
          }
        ),
      ),
    );
  }
}